import os

ETCD_HOST = os.environ.get("ETCD_HOST") or "localhost"
ETCD_PORT = os.environ.get("ETCD_HOSTPORT") or 2379
