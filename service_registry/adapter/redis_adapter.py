from service_registry.exceptions import SRException, SRKeyError
import threading
import redis as rd
from abc import abstractmethod
# from etcd3gw.exceptions import Etcd3Exception
# from service_registry.detect_etcd import detect_etcd
from service_registry.adapter.base_adapter import BaseAdapter
import threading
import logging


log = logging.getLogger(__name__)


class RedisAdapter(BaseAdapter):
    # @abstractmethod
    def __init__(self, namespace=""):
        self.namespace = namespace
        self.red = rd.Redis(host="localhost", port=6379, decode_responses=True)
        self.exit_flag = threading.Event()
        self.watch_threads = {}
        try:
            self.red.ping()
            if self.client is None:
                raise Exception
        except Exception as e:
            if not self.red:
                raise rd.DataError(
                    "Redis not available",
                    "Could not be detected"
                    # f"ETCD as a service is not running under the specified address "
                    # f"'{etcd_host}:{etcd_port}'.",
                )  # from e

    def _get_full_key(self, key):
        return f"{self.namespace}{key}"

    def get_apis(self, key_prefix):
        values = self.get_prefix(key_prefix=key_prefix)
        apis = []

        for key, value in values.items():
            if key.endswith("/api_name") and value != "":
                url_key = key[:-9] + "/url"
                if url_key in values:
                    apis.append({"name": value, "url": values[url_key]})
        return apis
    
    # @abstractmethod
    def put(self, key, value, ttl=-1):
        if ttl==-1:
            ttl = None
        try:
            self.red.set(name=self._get_full_key(key), value=value, ex=ttl)
            self.red.publish(self._get_full_key(key), value)

        # except:
        #     pass
        except rd.RedisError as e:
            raise SRException(
                "ETCD Error",
                f"Unable to create save key-value pair '{key}' - '{value}'. " f"Message: {e.__class__.__name__} - {e.detail_text}",
            ) from e

    def get_prefix_lite(self, prefix):
        # Initialize the cursor to 0
        cursor = 0

        # dict to store key-value pairs
        key_value_pairs = {}

        # Scan through keys matching the prefix
        while True:
            # Scan keys with the cursor and the given prefix
            cursor, keys = self.red.scan(cursor=cursor, match=f"{prefix}*")

            # Retrieve values for each key
            for key in keys:
                value = self.red.get(key)
                key_value_pairs[key] = value

            # Check if the cursor is 0, indicating the end of the scan
            if cursor == 0:
                break

        return key_value_pairs

    def get_all_light(self):
        all_keys = self.red.keys("*")

        # Retrieve values for each key
        key_value_dict = {key: self.red.get(key) for key in all_keys}

        return key_value_dict
    
    # @abstractmethod
    def get_prefix(self, key_prefix, remove_prefix=False):
        try:
            kv_pairs = {}

            if key_prefix == "":
                for k, v in self.get_all_light().items():
                    # key = str(k["key"], encoding="utf-8")[len(self._get_full_key("")) :]
                    # value = str(v, encoding="utf-8")
                    key = k
                    value = v
                    if remove_prefix:
                        kv_pairs[key.replace(key_prefix, "")] = value
                    else:
                        kv_pairs[key] = value
            else:
                for k, v in self.get_prefix_lite(
                    self._get_full_key(key_prefix)
                ).items():
                    print(k, v)
                    # key = str(k["key"], encoding="utf-8")[len(self._get_full_key("")) :]
                    # value = str(v, encoding="utf-8")
                    key = k
                    value = v
                    if remove_prefix:
                        kv_pairs[key.replace(key_prefix, "")] = value
                    else:
                        kv_pairs[key] = value
            if not kv_pairs:
                raise SRKeyError(f"No key with prefix '{key_prefix}' exists.")
            return kv_pairs
        except rd.DataError as e:
            raise SRException(
                "ETCD Error",
                f"Unable to find key_prefix: '{key_prefix}' "
                f"Message: {e.__class__.__name__} - {e.args}",
            ) from e
        
    # @abstractmethod
    def put_many(self, kv_pairs, ttl=-1):
        if ttl==-1:
            ttl = None
        try:
            lease = None
            if kv_pairs:
                for key, value in kv_pairs.items():
                    self.red.set(name=self._get_full_key(key), value=value, exat=ttl)
                    self.red.publish(self._get_full_key(key), value)
        except rd.DataError as e:
            raise SRException(
                "ETCD Error",
                f"Unable to create save key-value pair '{key}' - '{value}'. "
                f"Message: {e.__class__.__name__} - {e.args}",
            ) from e

    def delete_prefix_light(self, prefix):
        keys_to_delete = [key for key in self.red.keys(f"{prefix}*")]

        # Delete each key
        for key in keys_to_delete:
            self.red.delete(key)
            self.red.publish(key, self.red.get(key))

    # @abstractmethod
    def delete_prefix(self, prefix):
        try:
            self.delete_prefix_light(prefix)
        except rd.DataError as e:
            raise SRException(
                "ETCD Error",
                f"Unable to delete keys with prefix '{prefix}'. "
                f"Message: {e.__class__.__name__} - {e.args}",
            ) from e

    def watch(self, key, callback):
        pubsub = self.red.pubsub()

        def watch_thread():
            pubsub.subscribe(key)

            for message in pubsub.listen():
                if message["data"] == "§/§/§":
                    message["type"] = "delete"
                    callback(message["type"], key, message["data"])
                else:
                    callback(message["type"], key, message["data"])

                if self.exit_flag.is_set():
                    break

        def watch_cancel():
            pubsub.unsubscribe(key)

        thread = threading.Thread(target=watch_thread)
        self.watch_threads[key] = thread
        thread.start()

        return watch_cancel

    # @abstractmethod
    def get(self, key):
        try:
            res = self.red.get(self._get_full_key(key))
            if len(res) == 0:
                raise SRKeyError(f"The key '{key}' does not exist.")
            # value_str = str(res[0], encoding="utf-8")
            value_str = res
            try:
                return int(value_str)
            except ValueError:
                pass
            try:
                return float(value_str)
            except ValueError:
                pass
            return value_str

        except rd.DataError as e:
            raise SRException(
                "ETCD Error",
                f"Unable to obtain value for key '{key}'. "
                f"Message: {e.__class__.__name__} - {e.args}",
            ) from e

    # @abstractmethod
    def delete(self, key):
        try:
            self.red.delete(self._get_full_key(key))
            self.red.publish(key, "§/§/§")
        except rd.DataError as e:
            raise SRException(
                "ETCD Error",
                f"Unable to delete key '{key}'. "
                f"Message: {e.__class__.__name__} - {e.args}",
            ) from e

    # @abstractmethod
    def backend_alive(self):
        """Function to to get etcd status

        Returns
        -------
        bool
            True if etcd is accessible, False otherwise
        """
        try:
            result = self.red.ping()
            return True
        except Exception as e:
            print(e)
            return False


if __name__ == "__main__":
    sr = RedisAdapter()
    apis = sr.get_apis("demi")
    a = 1
