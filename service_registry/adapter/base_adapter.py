from abc import ABC, abstractmethod


class BaseAdapter(ABC):
    @abstractmethod
    def __init__(self, namespace=""):
        pass

    def get_suffix(self, suffix: str, prefix: str = ""):
        """Returns a list of all keys and their values starting with a given prefix and ending with a given suffix

        Parameters
        ----------
        suffix : string
            the suffix to search for
        prefix : string
            the prefix to search for (leave empty for all keys)



        Returns
        -------
        list of found apis
        """
        kv_pairs = self.get_prefix(prefix)
        output = {}

        for k, v in kv_pairs.items():
            if k.endswith(suffix):
                output[k] = v

        return output

    @abstractmethod
    def put(self, key: str, value: str, ttl=-1):
        """Set a key-value pair into the ServiceRegistry

        Parameters
        ----------
        key : string
            The key of the key-value pair.
        value : string, int, float
            The value of the key-value pair. Stored as string in the ServiceRegistry.
        ttl : int (optional, default: -1)
            Time to live. If not -1, key-value pair will only persist for <ttl> seconds.

        Returns
        -------
        None
        """
        pass

    @abstractmethod
    def get_prefix(self, key_prefix: str = "", remove_prefix=False):
        """Search for keys beginning with key_prefix in the ServiceRegistry

        Parameters
        ----------
        key_prefix : string
            the prefix to search for (leave empty for all keys)
        remove_prefix : boolean
            defines if the prefix is removed from returned keys

        Returns
        -------
        dict of found key-value-pairs
        """

        pass

    @abstractmethod
    def put_many(self, kv_pairs: str, ttl=-1):
        """Put multiple key-value-pairs from a dict into the ServiceRegistry

        Parameters
        ----------
        kv_pairs : dict
            A dictonary of the key-value pairs
        ttl : int (optional, default: -1)
            Time to live. If not -1, key-value pair will only persist for <ttl> seconds.

        Returns
        -------
        None
        """
        pass

    @abstractmethod
    def delete_prefix(self, prefix: str):
        """Delete every key/value pair in the ServiceRegistry, where the key starts with the prefix

        Parameters
        ----------
        prefix : string
            The prefix to delete all keys with.

        Returns
        -------
        None
        """
        pass

    # def watch(self, key : str, callback : function, prefix=False):
    @abstractmethod
    def watch(self, key: str, callback):
        """Watches Changes of one key or keys with a given Prefix.

        Parameters
        ----------
        key : string
            The key which has to be watched.
        callback(event_type, key, value) : function
            The wanted callback function, which has to use the type of the Event, the watched Key and the belonging Value of it.

        Returns
        -------
        watch_cancel : Used to cancel the watching of the key
        """
        pass

        # def watch_value(self, key: str, default_value=None):
        """ Returnes function that allows you

        Parameters
        ----------
        key : string
            The key which has to be watched.
        callback(event_type, key, value) : function
            The wanted callback function, which has to use the type of the Event, the watched Key and the belonging Value of it.
        prefix: Bool
            True = watching of keys with prefix as key, false watching of a single key
        Returns
        -------
        watch_cancel : Used to cancel the watching of the key/-s
        """
        # def watch_value(self, key, default_value=None):
        #     watcher, watch_cancel = self.watch(self._get_full_key(key))
        #     lock = threading.Lock()
        #     try:
        #         value = [self.get(key)]
        #     except SRException:
        #         value = [default_value]

        #     def watcher_thread(val):
        #         for event in watcher:
        #             with lock:
        #                 if "kv" in event and "value" in event["kv"]:
        #                     val[0] = str(event["kv"]["value"], encoding="utf-8")
        #                 else:
        #                     val[0] = None

        #     t1 = threading.Thread(target=watcher_thread, args=(value,))
        #     t1.start()

        #     def cancel():
        #         watch_cancel()
        #         t1.join()

        #     def get_value():
        #         with lock:
        #             return value[0]

        #     return get_value, cancel

    @abstractmethod
    def get(self, key: str):
        """Get value of a given key.

        Parameters
        ----------
        key : string
            The key for which the value should be obtained.

        Returns
        -------
        value: int, float, string
            Value of the key. String by default, parsed to int/float if possible.
        """
        pass

    @abstractmethod
    def delete(self, key: str):
        """Delete a given key together with its value.

        Will not (!) throw an exception, if the key does not exist.

        Parameters
        ----------
        key : string
            The key to be deleted.

        Returns
        -------
        None
        """
        pass

    @abstractmethod
    def backend_alive(self):
        """Function to to get backend status

        Returns
        -------
        bool
            True if backend is accessible, False otherwise
        """
        pass
