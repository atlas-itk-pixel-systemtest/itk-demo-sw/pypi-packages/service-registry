import os
import socket

# import sys

from etcd3gw.client import Etcd3Client

from dotenv import load_dotenv
import logging

load_dotenv()

log = logging.getLogger(__name__)
# print(f"Setting log level to {log_level}")
# log.setLevel(log_level)


def in_docker():
    """Check if running in docker.
    (Apparently using environment variables is preferred... tbd)
    """
    path = "/proc/self/cgroup"
    return (
        os.path.exists("/.dockerenv")
        or os.path.isfile(path)
        and any("docker" in line for line in open(path))
    )


def try_etcd(host):
    try:
        client = Etcd3Client(host=host, port=2379, api_path="/v3/")
        if client.status():
            log.info("Connection ok")
            return client
    # except ConnectionFailedError as e:
    except Exception as e:
        log.info("Could not connect to etcd")
        # log.exception(e)
        return None


def detect_etcd():
    # try environment variable
    host = os.environ.get("ETCD_HOST")
    if host:
        log.info(f"Trying host: {host} (from environment)")
        client = try_etcd(host)
        if client:
            return client

    if not in_docker():
        etcd_host_list = [
            "etcd",
            socket.gethostname(),
            "localhost",
            "127.0.0.1",
        ]

        for host in etcd_host_list:
            log.info(f"Trying host: '{host}'")
            client = try_etcd(host)
            if client:
                return client
    else:
        log.info("Seems like we're in a Docker container...")
        etcd_docker_host_list = [
            "etcd",
            socket.gethostname(),
            "demitools-etcd-1",
            "host.docker.internal",
            "host.containers.internal",
        ]

        for host in etcd_docker_host_list:
            log.info(f"Trying: '{host}'")
            client = try_etcd(host)
            if client:
                return client

    return None
